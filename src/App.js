import React, { useState, useEffect } from "react";
import "./App.css";
import Axios from "axios";

function App() {
  const [food, setFood] = useState("");
  const [days, setDays] = useState(0);
  const [foodList, setFoodList] = useState([]);
  const [newFoodName, setNewFoodName] = useState("");

  useEffect(() => {
    Axios.get("http://localhost:3001/read").then((response) => {
      setFoodList(response.data);
    });
  }, []);

  const addToList = () => {
    Axios.post("http://localhost:3001/insert", { food: food, days: days });
  };

  const updateFood = (id) => {
    Axios.put("http://localhost:3001/update", {
      id: id,
      newFoodName: newFoodName,
    });
  };

  const deleteFood = (id) => {
    Axios.delete(`http://localhost:3001/delete/${id}`);
  };

  return (
    <div className="App">
      <h1>CRUD</h1>
      <h2>(Create,Read,Update,Delete)</h2>
      <label>Food Item: </label>
      <input
        className="form-text"
        type="string"
        onChange={(event) => {
          setFood(event.target.value);
        }}
      />
      <label> Days Since I Ate: </label>
      <input
        className="form-text"
        type="number"
        onChange={(event) => {
          setDays(event.target.value);
        }}
      />
      <button className="btn btn-primary m-2" onClick={addToList}>
        Add to list
      </button>
      <h1 className="m-5">Food List</h1>
      {foodList.map((val, key) => {
        return (
          <div key={key}>
            <table className="table table-hover">
              {/* <thead>
                <tr>
                  <th>Food Items</th>
                  <th>Days</th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </thead> */}
              <tbody>
                <tr>
                  <td>{val.foodItem}</td>
                  <td>{val.daysSinceIAte}</td>
                  <td>
                    <input
                      className="form-control"
                      type="text"
                      onChange={(event) => {
                        setNewFoodName(event.target.value);
                      }}
                    ></input>
                  </td>
                  <td>
                    <button
                      className="btn btn-primary"
                      onClick={() => updateFood(val._id)}
                    >
                      Update
                    </button>
                  </td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={() => deleteFood(val._id)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        );
      })}
    </div>
  );
}

export default App;
